import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        Integer[] primeNumbers = {2, 3, 5 , 7, 11};
        System.out.println("The first prime number is: " + primeNumbers[0]);
        System.out.println("The second prime number is: " + primeNumbers[1]);
        System.out.println("The third prime number is: " + primeNumbers[2]);
        System.out.println("The fourth prime number is: " + primeNumbers[3]);
        System.out.println("The fifth prime number is: " + primeNumbers[4]);

        ArrayList<String> friends = new ArrayList<String>(Arrays.asList("Nicko", "Andrei", "Camille", "Allana"));
        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>(){
            {
                put("Piattos", 28);
                put("Nova", 50);
                put("Vcut", 25);
                put("Pillows", 39);
                put("Pancit Canton", 21);
            }
        };

        System.out.println("Our current Inventory consists of: " + inventory);

    }
}